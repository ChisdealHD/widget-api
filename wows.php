<?php 

    
    $_GET['user'];

    $username = $_GET['user'];

    $page = $_SERVER['PHP_SELF']. "?user=".$username;
    $sec = "60";

    header("Refresh: $sec; url=$page");
    $url = "https://" . $_SERVER['HTTP_HOST'] . "/api/wowsapi.php?user=".$username;
    $content = file_get_contents($url);
    $json = json_decode($content, true);
    
    //Shortcuts
    $pvp = $json['WOWS']['pvp'];
    $pve = $json['WOWS']['pve'];
    $ranked = $json['WOWS']['ranked'];

    //callouts

    $pvpdata = "XP: " . $pvp['xp'] . ", Battles: " . $pvp['battles'] . ",<br> Wins: " . $pvp['wins'] . ", Losts: " . $pvp['losses'];

    $pvedata = "XP: " . $pve['xp'] . ", Battles: " . $pve['battles'] . ",<br> Wins: " . $pve['wins'] . ", Losts: " . $pve['losses'];

    $rankeddata = "XP: " . $ranked['xp'] . ", Battles: " . $ranked['battles'] . ",<br> Wins: " . $ranked['wins'] . ", Losts: " . $ranked['losses'];
  
?>


<!DOCTYPE html>
<html lang="en"><head>

  <meta charset="UTF-8"> 
  
  
<style>
@import url("https://fonts.googleapis.com/css?family=Source+Sans+Pro");
body {
  padding: 0;
  margin: 0;
}
body .nowplayingcard {
  min-width: 400px;
  max-width: 40%;
  font-family: "Source Sans Pro", sans-serif;
  font-size: 13px;
  background-color: #ffffff;
}
body .nowplayingcard .nowplayingcontainer-inner {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  display: inline-block;
  -webkit-border-top-left-radius: 3px;
  -moz-border-top-left-radius: 3px;
  -ms-border-top-left-radius: 3px;
  -o-border-top-left-radius: 3px;
  border-top-left-radius: 3px;
  -webkit-border-bottom-left-radius: 3px;
  -moz-border-bottom-left-radius: 3px;
  -ms-border-bottom-left-radius: 3px;
  -o-border-bottom-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
body .nowplayingcard .nowplayingcontainer-inner:hover {
  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
}
body .nowplayingcard .nowplayingcontainer-inner img#trackart {
  max-width: 30%;
  float: left;
  left: 0;
  -webkit-border-top-left-radius: 3px;
  -moz-border-top-left-radius: 3px;
  -ms-border-top-left-radius: 3px;
  -o-border-top-left-radius: 3px;
  border-top-left-radius: 3px;
  -webkit-border-bottom-left-radius: 3px;
  -moz-border-bottom-left-radius: 3px;
  -ms-border-bottom-left-radius: 3px;
  -o-border-bottom-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
body .nowplayingcard .nowplayingcontainer-inner .trackInfo {
  width: 70%;
  float: left;
  display: block;
}
body .nowplayingcard .nowplayingcontainer-inner .trackInfo a {
  max-width: 90%;
  display: block;
  font-size: 14px;
  text-align: left;
  text-decoration: none;
  vertical-align: middle;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
body .nowplayingcard .nowplayingcontainer-inner .trackInfo a:nth-child(odd) {
  color: black;
  font-weight: bold;
  vertical-align: middle;
  line-height: 15px;
  letter-spacing: 0.2px;
  padding: 10% 0 0 5%;
}
body .nowplayingcard .nowplayingcontainer-inner .trackInfo a:nth-child(odd) img {
  width: 15px;
  height: 15px;
  vertical-align: middle;
  margin: -2% 3px 0 0;
}
body .nowplayingcard .nowplayingcontainer-inner .trackInfo a:nth-child(even) {
  color: #444444;
  font-size: 12px;
  letter-spacing: 0.1px;
  padding: 5% 0 0 5%;
}
body .nowplayingcard .nowplayingcontainer-inner .trackInfo a:nth-child(even) img {
  width: 15px;
  height: 15px;
  vertical-align: middle;
  margin: -2% 3px 0 0;
}

.Container {
	float: left;
	height: 90px;
	width: 400px;
}
.data {
	float: left;
	height: 50px;
	width: 400px;
}

#slideshow > div {
  position: absolute;
  color: #7CFC00;
}


</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>

<div class="Container">
	<div class="data">
    	<div id="slideshow">
   		<div>

  			<div class="nowplayingcard">
				<div class="nowplayingcontainer-inner">
					<img id="trackart" src="https://wows-static-production.gcdn.co/wowsp/a3682aae/assetsV2/b53da64f6465b7563420825e6076f7ff.png">
					<div class="trackInfo">
						<a id="tracktitle" href="#" title="" target="_blank"><img src="https://wows-static-production.gcdn.co/wowsp/a3682aae/assetsV2/b53da64f6465b7563420825e6076f7ff.png">World of Warships (PVP)</a>
						<a href="#" id="trackartist" title=""><?php echo $pvpdata; ?></a>
						<a href="#" id="trackartist" title=""><img src="">Powered by ChisdealHDAPP</a>
					</div>
				</div>
			</div>
		</div>

		<div>

  			<div class="nowplayingcard">
				<div class="nowplayingcontainer-inner">
					<img id="trackart" src="https://wows-static-production.gcdn.co/wowsp/a3682aae/assetsV2/b53da64f6465b7563420825e6076f7ff.png">
					<div class="trackInfo">
						<a id="tracktitle" href="#" title="" target="_blank"><img src="https://wows-static-production.gcdn.co/wowsp/a3682aae/assetsV2/b53da64f6465b7563420825e6076f7ff.png">World of Warships (PVE)</a>
						<a href="#" id="trackartist" title=""><?php echo $pvedata; ?></a>
						<a href="#" id="trackartist" title=""><img src="">Powered by ChisdealHDAPP</a>
					</div>
				</div>
			</div>
    </div>
    
		<div>

    <div class="nowplayingcard">
				<div class="nowplayingcontainer-inner">
					<img id="trackart" src="https://wows-static-production.gcdn.co/wowsp/a3682aae/assetsV2/b53da64f6465b7563420825e6076f7ff.png">
					<div class="trackInfo">
						<a id="tracktitle" href="#" title="" target="_blank"><img src="https://wows-static-production.gcdn.co/wowsp/a3682aae/assetsV2/b53da64f6465b7563420825e6076f7ff.png">World of Warships (RANKED)</a>
						<a href="#" id="trackartist" title=""><?php echo $rankeddata; ?></a>
						<a href="#" id="trackartist" title=""><img src="">Powered by ChisdealHDAPP</a>
					</div>
				</div>
			</div>
		</div>
</div>
<script>
$("#slideshow > div:gt(0)").hide();

setInterval(function() {
  $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
}, 20000);
</script>
 
</body></html>