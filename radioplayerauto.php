<?php

$_GET['radio'];
$radiopath = $_GET['radio'];


$url = "https://api.chisdealhd.co.uk/v1/radiopublic/".$radiopath;
$content = file_get_contents($url);
$json = json_decode($content, true);

$error = $json['status'];

if ($json['status'] != 404){
	$radioURL = $json['radioURL'];
} else {
	$radioURL = "null";
}

?>


<!DOCTYPE html>



<!--



HOW TO CREATE AN AUDIO PLAYER [TUTORIAL]



"How to create an Audio Player [Tutorial]" was specially made for DesignModo by our friend Valeriu Timbuc.



Links:

http://vtimbuc.net/

https://twitter.com/vtimbuc

http://designmodo.com

http://vladimirkudinov.com



-->



<html lang="en">



<head>



	<meta charset="utf-8">



	<title>How to Create an Audio Player [Tutorial]</title>



	<!-- Audio Player CSS & Scripts -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

	<script src="radioplayerassets/js/mediaelement-and-player.min.js"></script>

	<link rel="stylesheet" href="radioplayerassets/css/style.css" media="screen">

	<!-- end Audio Player CSS & Scripts -->



	<style type="text/css">

		html, body { margin: 0;	padding: 0; }

		

		.audio-player { margin: 0 auto;}

	</style>

</head>

<script>

		function radioTitle() {

 

		jQuery.ajax({

	        url: "<?php echo $url; ?>",

	        //force to handle it as text

	        dataType: "text",

	        async: true,

	        success: function(data) {

	                        

	        var json = jQuery.parseJSON(data);



	        jQuery('#track-title').text(json.nowPlaying.song);


		    jQuery('#track-cover').html('<img class="cover" src="' + json.nowPlaying.art + '" alt="" width="120" hight="120" />');



	       }

	     });

	   }

	   jQuery(document).ready(function(){



	        setTimeout(function(){radioTitle();}, 1000);

	        // we're going to update our html elements / player every 15 seconds

	        setInterval(function(){radioTitle();}, 1000); 



		});



	</script>

<body>



	<!-- Audio Player HTML -->

	<div class="audio-player">

		<h1 id="track-title">Connecting to Radio API</h1>

		<p id="track-cover"> </p>

	</div>

	<!-- end Audio Player HTML -->



</body>



</html>