<?php


$_GET['user'];

$username = $_GET['user'];

$wows = "https://api.chisdealhd.co.uk/v1/game/wg/wows/".$username;
$content = file_get_contents($wows);
$json = json_decode($content, true);

//shortcuts
$wowspvp = $json['statistics']['pvp'];
$wowspve = $json['statistics']['pve'];
$wowsranksolo = $json['statistics']['rank_solo'];


header("Content-Type: application/json");
header('Access-Control-Allow-Origin: *');


if ($json['error'] != 404) {

$ar = [];
$ar['WOWS'] = [];

$ar["WOWS"]["stats"]["updated_time"] = $json['stats_updated_at'];
$ar["WOWS"]["stats"]["last_on"] = $json['logout_at'];
$ar["WOWS"]["stats"]["first_joined"] = $json['created_at'];
$ar["WOWS"]["stats"]["lvl_tier"] = $json['leveling_tier'];
$ar["WOWS"]["stats"]["lvl_points"] = $json['leveling_points'];
$ar["WOWS"]["stats"]["last_battle"] = $json['last_battle_time'];


//PVP ZONE
$ar["WOWS"]["pvp"]["xp"] = $wowspvp['xp'];
$ar["WOWS"]["pvp"]["survived_battles"] = $wowspvp['survived_battles'];
$ar["WOWS"]["pvp"]["ships_spotted"] = $wowspvp['ships_spotted'];
$ar["WOWS"]["pvp"]["battles"] = $wowspvp['battles'];
$ar["WOWS"]["pvp"]["wins"] = $wowspvp['wins'];
$ar["WOWS"]["pvp"]["losses"] = $wowspvp['losses'];
$ar["WOWS"]["pvp"]["max_planes_killed"] = $wowspvp['max_planes_killed'];
$ar["WOWS"]["pvp"]["torpedoes"] = $wowspvp['torpedoes']['hits'];



//PVE ZONE
$ar["WOWS"]["pve"]["xp"] = $wowspve['xp'];
$ar["WOWS"]["pve"]["survived_battles"] = $wowspve['survived_battles'];
$ar["WOWS"]["pve"]["ships_spotted"] = $wowspve['ships_spotted'];
$ar["WOWS"]["pve"]["battles"] = $wowspve['battles'];
$ar["WOWS"]["pve"]["wins"] = $wowspve['wins'];
$ar["WOWS"]["pve"]["losses"] = $wowspve['losses'];
$ar["WOWS"]["pve"]["max_planes_killed"] = $wowspve['max_planes_killed'];
$ar["WOWS"]["pve"]["torpedoes"] = $wowspve['torpedoes']['hits'];



//PVE ZONE
$ar["WOWS"]["ranked"]["xp"] = $wowsranksolo['xp'];
$ar["WOWS"]["ranked"]["survived_battles"] = $wowsranksolo['survived_battles'];
$ar["WOWS"]["ranked"]["ships_spotted"] = $wowsranksolo['ships_spotted'];
$ar["WOWS"]["ranked"]["battles"] = $wowsranksolo['battles'];
$ar["WOWS"]["ranked"]["wins"] = $wowsranksolo['wins'];
$ar["WOWS"]["ranked"]["losses"] = $wowsranksolo['losses'];
$ar["WOWS"]["ranked"]["max_planes_killed"] = $wowsranksolo['max_planes_killed'];
$ar["WOWS"]["ranked"]["torpedoes"] = $wowsranksolo['torpedoes']['hits'];

echo json_encode($ar);

} else {

	$ar = [];

	$ar["error"] = 404;
	$ar["message"] = (string) "Username not Found!";
	echo json_encode($ar);
}
?>